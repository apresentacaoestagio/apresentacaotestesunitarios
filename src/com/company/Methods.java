package com.company;

public class Methods {

    ////////////////////////////////////////////////
    // Alexandre Varela & Alexandre Hilário 2020  //
    ////////////////////////////////////////////////

    /**
     * Método onde é devolvido o nome introduzido
     * @param name primeiro número
     * @return devolve o nome
     */
    public static String example(String name){
        return name;
    }

    /**
     * Método onde é simulado um login
     * @param username primeiro número
     * @param password segundo número
     * @return devolve o resultado do login
     */
    public static String login(String username, String password){
        if(username.equals("user123") && password.equals("password2020")) return "pass";
        else return "fail";
    }

    /**
     * Método onde é feita a soma de dois números
     * @param num1 primeiro número
     * @param num2 segundo número
     * @return devolve o resultado da soma
     */
    public static int sum(int num1, int num2){
        return num1 + num2;
    }

    /**
     * Método onde é feita a divisão de dois números
     * @param num1 primeiro número
     * @param num2 segundo número
     * @return devolve o resultado da divisão
     */
    public static int div(int num1, int num2) {
        return num1 / num2;
    }
}
